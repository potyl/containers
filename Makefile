SHELL := bash

COLOR_START     = \e[91m\e[1m
COLOR_END       = \e[0m
SAY             = @printf "$(COLOR_START)%s\n$(COLOR_END)"

# The container name based on the current tartget (remove the extenstion from the target name)
CONTAINER =	$(@:$(suffix $@)=)

# All containers that we can deploy
CONTAINERS := $(shell dirname $$(find * -name docker-compose.yaml | sort))

# Build a corresponding up and down target for each container
TARGETS_UP := $(CONTAINERS:%=%.up)
TARGETS_DOWN := $(CONTAINERS:%=%.down)
TARGETS_LOGS := $(CONTAINERS:%=%.logs)
TARGETS_RESTART := $(CONTAINERS:%=%.restart)


##
## Ad hoc commands
##

.PHONY: info
info:
	$(SAY) "Docker containers"
	docker compose ls
	@echo

	$(SAY) "Containers:"
	@for container in $(CONTAINERS); do echo $$container; done
	@echo

	@echo $(a)


all: $(TARGETS_UP)

##
## Containers targets
##

.PHONY: $(CONTAINERS)
$(CONTAINERS):
	@$(MAKE) $@.up

.PHONY: $(TARGETS_UP)
$(TARGETS_UP):
	$(eval IP := $(shell perl -le 'print join ".", unpack C4 => gethostbyname shift;' "$(CONTAINER).home"))
	$(SAY) "Starting container $(CONTAINER) on IP $(IP)"
	IP=$(IP) docker compose --project-directory $(CONTAINER) up --detach

.PHONY: $(TARGETS_DOWN)
$(TARGETS_DOWN):
	$(SAY) "Stoping container $(CONTAINER)"
	docker compose --project-directory $(CONTAINER) down

.PHONY: $(TARGETS_LOGS)
$(TARGETS_LOGS):
	$(SAY) "Logs for container $(CONTAINER)"
	docker compose --project-directory $(CONTAINER) logs --tail 10 --follow

.PHONY: $(TARGETS_RESTART)
$(TARGETS_RESTART):
	$(eval IP := $(shell perl -le 'print join ".", unpack C4 => gethostbyname shift;' "$(CONTAINER).home"))
	$(SAY) "Restarting $(CONTAINER) on IP $(IP)"
	IP=$(IP) docker compose --project-directory $(CONTAINER) restart
